﻿using UnityEngine;
using System.Collections;

public enum StatTypes
{
    LVL, // Level
    EXP, // Experience
    AC, // Armor Class
    HP,  // Hit Points
    MHP, // Max Hit Points
    PRO, // Proficiency
    L1SS,  // Level 1 Spell Slots
    ML1SS, // Max Level 1 Spell Slots
    L2SS,  // Level 2 Spell Slots
    ML2SS, // Max Level 2 Spell Slots
    L3SS,  // Level 3 Spell Slots
    ML3SS, // Max Level 3 Spell Slots
    STR, // Strenght
    DEX, // Dexterity
    CON, // Consititution
    INT, // Intelligence
    WIS, // Wisdom
    CHA, // Charisma
    SPD, // Speed
    Count
}
