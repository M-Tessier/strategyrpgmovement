﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum EquipSlots
{
    None = 0,
    RightHand = 1 << 0,
    LeftHand = 1 << 1,
}
