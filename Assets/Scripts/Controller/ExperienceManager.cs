﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class ExperienceManager
{
    public static void AwardExperience(int amount, Rank rank)
    {
            rank.EXP += amount;
    }
}
